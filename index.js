console.log('Hello World')

// ======SOLUTION=======
let number = Number(prompt('Enter a number: '));
console.log('The number the your provided is '+ number);

for (let count = number; count >= 0; count--){
	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop");
		break;
	} else if (count % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number!"); continue;
	} else if (count % 5 === 0){
		console.log(count);
	}
}



let word = "supercalifragilisticexpialidocious";
let oneLineWord = '';
console.log(word)

for(let i = 0; i < word.length; i++){

	// If the character of your name is a vowel letter, insted of displaying character, display 3
	// The .toLowerCase() will froce string to go lower case.

	if(
		word[i].toLowerCase() == "a" ||
		word[i].toLowerCase() == "e" ||
		word[i].toLowerCase() == "i" ||
		word[i].toLowerCase() == "o" ||
		word[i].toLowerCase() == "u"     

		){
		// If the current letter is a vowel then skip that
		continue;
	}else{
		oneLineWord+=word[i];
	}
}
console.log(oneLineWord);